import React from 'react';
import api from "../../services/axios";
import { BrowserRouter as Router,  Route, Routes, Switch } from 'react-router-dom';
import ReactDOM from 'react-dom';
import HeaderSidebar from "../admin/layouts/HeaderDashboard";
import Nav from "../admin/layouts/Nav";
import Footer from './layouts/Footer';
//Reducer
import { store } from '../reducer/store';
import { Provider } from 'react-redux';
//module
import Vendedores from "./Vendedores/Index";
import Productos from "./Productos/Index";
import Categorias from "./Productos/Categorias/Index";
import Subcategorias from './Productos/Subcategorias/Index';

function Admin() {

    return (
        <Provider store={store}>
            <Router>
            <div id="wrapper">
                <Nav />
                <div id="content-wrapper" class="d-flex flex-column">
                    <div id="content">
                        <HeaderSidebar />
                        <div class="container-fluid">

                            <Switch>
                                <Route path="/admin/vendedores/index" > <Vendedores /> </Route>
                                <Route path="/admin/productos/index" > <Productos /> </Route>
                                <Route path="/admin/productos/categorias/index" > <Categorias /> </Route>
                                <Route path="/admin/productos/subcategorias/index" > <Subcategorias /> </Route>
                            </Switch>
                        </div>

                    </div>
                <Footer />
                </div>
            </div>
            </Router>
        </Provider>
    );
}

export default Admin;

if (document.getElementById('admin')) {
    ReactDOM.render(<Admin />, document.getElementById('admin'));
}

