import React, { useState, useEffect } from 'react';
import { Button, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

import Form from "./Form"
import api from "../../../../services/axios"

function Categoria() {

    /* const [ listCategoria, setListCategorie ] = useState([]);
    const [ listSubcat, setListSubcatg ] = useState([]);
    const [ dataCategorie, setCategorie ] = useState({});
    const [ form, setForm ] = useState({
        edit: false,
        show:false
    }); */

    const [ id, setId ] = useState(null);
    const [ data, setData ] = useState({});
    const [ show, setShow ] = useState(false);
    const [ validar, setValidar ] = useState(false);
    const [ isLoading, setIsLoading ] = useState(false);
    const [ listCategoria, setListCategoria ] = useState([]);

    useEffect(()=>{ onGetData(); },[])

    const handleClose = () =>{
        setShow(false);
        setValidar(false)
        setId (null);
        onGetData();
    }

    const modal = async () => {
        setShow(true);
    }

    const onGetData = (  ) => {

        api.get("category/get")
        .then((res)=>{
            if(res.success){
                setListCategoria(res.data)
            }
        })
        .catch((error)=>{
            console.log("Error de catch");
            console.log(error.message);
            console.log(error);
        })

    }

    const dataUpdate = async (data, valor) => {
        setIsLoading(true)
        console.log("Aqui dataUpdate")
        console.log(data);
    
        var nombre = data.nombre
        var imagen = data.imagen
        var estado = data.estado
    
        const dataForAll = {
            nombre,
            imagen,
            estado,
        }
        setValidar(valor)
        await setId(data.id)
        await setData(dataForAll)
        await setShow(true);
        console.log("Aqui data forAll")
        console.log(dataForAll);
        setIsLoading(false)
        setData({})
    }

    const onClickDelete = (data) =>{
        Swal.fire({
          title: `¿ Quieres eliminar esta categoría ${data.nombre}?`,
          showDenyButton: true,
          confirmButtonText: `Sí`,
          denyButtonText: `No`,
    
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            const res = api.delete("category/"+data.id)
            .then(res=>{
                Swal.fire('Categoría eliminada correctamente!', '', 'success')
                onGetData();
            })
    
          } else if (result.isDenied) {
            Swal.fire('Acción cancelada', '', 'info')
          }
        })
    }

    return (
      <div class="row">
        <div class="col-md-12">
            <h3 class="title-5 m-b-35">Categorías</h3>


            <button type="button" class="btn btn-primary"  onClick = {()=>modal()}>
             <i class="fa fa-map-marker"></i>&nbsp; Agregar categoría
            </button>

            <hr/>

        </div>



        <div class="col-md-12">
            <div class="table-responsive table--no-card m-b-30">
                <table class="table table-borderless table-striped table-earning  table-data3 border-bottom-primary">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Imagen</th>
                            <th>Nombre</th>
                            <th>Estado</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            listCategoria.map((item)=>{
                                return(
                                <tr>
                                    <td>{item.id}</td>
                                    <td>

                                        <img src={"/storage/"+item.imagen}  alt={item.nombre} style={{width:100}}/>

                                    </td>
                                    <td>{item.nombre}</td>
                                    <td style={item.estado == 1 ? {color:'#00ad5f'} : {color: 'red'}}>{item.estado?"Activo":"Inactivo"}</td>
                                    <td>
                                        <div style={{display: 'flex'}}>
                                        <button className="btn btn-success btn-circle sm" onClick = {()=>dataUpdate(item, true)} style={{marginRight: '10px'}}><i class="fas fa-pen fa-sm"></i></button>
                                        <button className="btn btn-danger btn-circle sm" onClick = {()=>onClickDelete(item)} ><i class="fas fa-trash fa-sm"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>

        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>{id > 0 ? 'Editar categoría':'Agregar categoría'}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form handleClose={()=>handleClose()} data={data} nom={validar} id = {id}/>
            </Modal.Body>
        </Modal>


        </div>
    );
}

export default Categoria;