import React, { useState, useEffect  }  from 'react';
import makeAnimated from 'react-select/animated'
import Swal from 'sweetalert2';
import { Button, Modal } from 'react-bootstrap';

import api from "../../../../services/axios";

function Form(props) {

    const [loading, setLoading] = useState(false)

    const [ animatedComponents, setAnimatedComponents ] = useState(makeAnimated)
    const [ id, setId ] = useState(0);
    const [ nombre, setNombre ] = useState(null);
    const [ active, setActive ] = useState(true);
    const [ image, setImage ] = useState("/assets/image-not-found.png");
    const [ fileImage, setFileImage ] = useState(null);
    const [ error, setError ] = useState(null);


    useEffect(()=>{
        validar();
    }, []);

    const MessageError = async (data) => {
        Swal.fire({
          title: 'Error',
          text: data,
          icon: 'warning',
        })
    }
    
    const MessageSuccess = async (data) => {
        Swal.fire({
          text: data,
          icon: 'success',
        })
    }

    const validar = async () =>{
        if(props.id > 0){
            setNombre(props.data.nombre)
            setActive(props.data.estado)
            setImage("/storage/"+props.data.imagen)
        }
    }

    const onClickSave = async() => {
        setLoading(true)
        setError("")
        var message = ''
        var error = false
        if (nombre == null){
          error = true
          message = "Escribe el nombre de la categoría "
        }else if(fileImage == null){
          error = true
          message = "Elige la imagen que corresponde a la categoría "
        }
        if(error){
            MessageError(message)
        }
        else{
          const data = new FormData()
          data.append('id',id)
          data.append('nombre', nombre)
          data.append('estado', active?1:0);
          if(fileImage){ data.append("imagen",fileImage); }
          api.post("category/create",data)
            .then(res=>{
                console.log("Resultado de entonces");
                console.log(res.message);
                console.log(JSON.stringify(res));
                if(res.success){
                    MessageSuccess("Categoría creada correctamente");
                    props.handleClose();
                }else if(props.id > 0){
                    MessageSuccess("Categoría editado correctamente");
                    props.handleClose();
                }
                else{
                    setError(JSON.stringify(res.message));
                    setLoading(false);
                    props.handleClose();
                }
            })
            .catch((error)=>{
                console.log("Error de catch");
                console.log(error);
                setError(res.message)
            })
        }
    }

    const setUploadImage = (event) => {
        setImage(URL.createObjectURL(event.target.files[0]));
        setFileImage(event.target.files[0]);
    }

    return(
        <div>
     
            <div class="container-fluid">
                <div class="row">
                    <div class="col col-md-8 mb-3">
                        <label for="recipient-name" className="col-form-label">Nombre:</label>
                        <input type="text" className="form-control" placeholder="Ingrese el nombre de la categoría" value={nombre} onChange={(event)=>setNombre(event.target.value)}/>
                    </div>
                    <div class="col col-md-8 mb-3">
                        <label for="text-input" class=" form-control-label">Estado</label>
                        <select name="select" id="select" class="form-control"
                            value={active} onChange={(event)=>setActive(event.target.value)}>
                            <option value="1">Activada</option>
                            <option value="0">Desactivada</option>
                        </select>
                    </div>
                    <div class="col col-md-8 mb-3">
                        <label for="text-input" class=" form-control-label">Imagen: </label>
                        <input type="file" id="file-input" name="file-input" class="form-control-file" onChange={(e)=>setUploadImage(e)} />
                    </div>
                </div>
            </div>
            <Modal.Footer>
                <Button variant="primary" onClick = {()=>onClickSave()}>Guardar categoría</Button>
                <Button variant="danger" onClick = {()=>props.handleClose()}>Cerrar</Button>
            </Modal.Footer>
        </div>
    );
}

export default Form;