import React, { useState, useEffect  }  from 'react';
import makeAnimated from 'react-select/animated'
import Swal from 'sweetalert2';
import { Button, Modal } from 'react-bootstrap';
import Select from 'react-select';

import api from "../../../services/axios";
import { useSelector, useDispatch } from 'react-redux'
import { setCategorias } from '../../reducer/categoria/categoriaSlice';
import { setSubcategorias } from '../../reducer/subCategoria/subCategoriaSlice'

function Form(props) {

    const [loading, setLoading] = useState(false)
    const [ animatedComponents, setAnimatedComponents ] = useState(makeAnimated)
    const [ id, setId ] = useState(0);
    const [ nombre, setNombre ] = useState(null);
    const [ precio, setPrecio ] = useState(null);
    const [ categoria, setCategoria ] = useState(null);
    const [ categoriaSelect , setCategoriaSelect ] = useState([]);

    const [ subcategoriaSelect, setSubcategoriaSelect ] = useState(null);
    const [ active, setActive ] = useState(true);
    const [ image, setImage ] = useState("/assets/image-not-found.png");
    const [ fileImage, setFileImage ] = useState(null);
    const [ error, setError ] = useState(null);



    //const categoria = useSelector(state => state.categorias)
    const dispatch = useDispatch()
    const categorias = useSelector(state => state.categorias.list)
    const subcategorias = useSelector(state => state.subCategorias.list)

    const getCategory = async () => {
        api
        .get('category/get')
        .then(response=>{
            var res = response.data;
            var catego = [];
            res.map(item=>{
                const data = {
                    value: item.id,
                    label: item.nombre
                }
                catego.push(data);
                subCategoria(item.id, false)

            })
            dispatch(setCategorias(catego))

            if (props.id > 0){
                console.log(props);

                console.log(props.data.categoria_id);
                let filtro = res.filter(e=>e.id == props.data.categoria_id);
                console.log(filtro);
                const data = {
                    value: filtro[0].id,
                    label: filtro[0].nombre
                }
                setCategoriaSelect(data)
            }
        })
        .catch(error=>{
                console.log(error);
        })
    }

    const subCategoria =(data)=>{
        setCategoriaSelect(data);
        api
        .get("subcategory/get")
        .then(response=>{
            var res = response.data;
            var catego = [];
            let filtro = res.filter(e=>e.categoria_id == data.value);
            console.log("breee",filtro);
            filtro.map(item=>{
                const data = {
                    value: item.id,
                    label: item.nombre
                }
                catego.push(data);
            })
            dispatch(setSubcategorias(catego))

        })
    }

    useEffect(()=>{
        validar();
        getCategory()
        //dispatch(s());
    }, []);

    const MessageError = async (data) => {
        Swal.fire({
          title: 'Error',
          text: data,
          icon: 'warning',
        })
    }

    const MessageSuccess = async (data) => {
        Swal.fire({
          text: data,
          icon: 'success',
        })
    }



    const validar = async () =>{
        if(props.id > 0){
            setNombre(props.data.nombre)
            setActive(props.data.estado)
            setImage("/storage/"+props.data.imagen)
        }
    }

    const onClickSave = async() => {
        setLoading(true)
        setError("")
        var message = ''
        var error = false
        if (nombre == null){
          error = true
          message = "Escribe el nombre del producto "
        }else if(fileImage == null){
          error = true
          message = "Elige la imagen que corresponde a la categoría "
        }
        if(error){
            MessageError(message)
        }
        else{
          const data = new FormData()
          data.append('id',id)
          data.append('nombre', nombre)
          data.append('estado', active?1:0);
          if(fileImage){ data.append("imagen",fileImage); }
          api.post("productos/category/create",data)
            .then(res=>{
                console.log("Resultado de entonces");
                console.log(res.message);
                console.log(JSON.stringify(res));
                if(res.success){
                    MessageSuccess("Categoría creada correctamente");
                    props.handleClose();
                }else if(props.id > 0){
                    MessageSuccess("Categoría editado correctamente");
                    props.handleClose();
                }
                else{
                    setError(JSON.stringify(res.message));
                    setLoading(false);
                    props.handleClose();
                }
            })
            .catch((error)=>{
                console.log("Error de catch");
                console.log(error);
                setError(res.message)
            })
        }
    }

    const setUploadImage = (event) => {
        setImage(URL.createObjectURL(event.target.files[0]));
        setFileImage(event.target.files[0]);
    }

    return(
        <div>

            <div class="container-fluid">

                <div class="row">
                    <div class="col col-md-8 mb-3">
                        <label for="recipient-name" className="col-form-label">Nombre:</label>
                        <input type="text" className="form-control" placeholder="Ingrese el nombre del producto" value={nombre} onChange={(event)=>setNombre(event.target.value)}/>
                    </div>
                    <div class="col col-md-8 mb-3">
                        <label for="recipient-name" className="col-form-label">Precio:</label>
                        <input type="text" className="form-control" placeholder="Ingrese el Precio del producto" value={precio} onChange={(event)=>setPrecio(event.target.value)}/>
                    </div>
                    <div className="col col-md-8 mb-3">
                        <label htmlFor="recipient-name" className="col-form-label">Categoria:</label>
                         <Select
                        value={categoriaSelect}
                        closeMenuOnSelect={true}
                        components={animatedComponents}
                        options={categorias}
                        onChange={(e)=>subCategoria(e)}
                        placeholder = "Seleccionar categoria"
                        name="colors"
                        />
                    </div>
                    <div className="col col-md-8 mb-3">
                        <label htmlFor="recipient-name" className="col-form-label">SubCategoria:</label>
                         <Select
                        value={subcategoriaSelect}
                        closeMenuOnSelect={true}
                        components={animatedComponents}
                        options={subcategorias}
                        onChange={(e)=>setSubcategoriaSelect(e)}
                        placeholder = "Seleccionar categoria"
                        name="colors"
                        />
                    </div>
                    <div class="col col-md-8 mb-3">
                        <label for="text-input" class=" form-control-label">Estado</label>
                        <select name="select" id="select" class="form-control"
                            value={active} onChange={(event)=>setActive(event.target.value)}>
                            <option value="1">Activada</option>
                            <option value="0">Desactivada</option>
                        </select>
                    </div>
                    <div class="col col-md-8 mb-3">
                        <label for="text-input" class=" form-control-label">Imagen: </label>
                        <input type="file" id="file-input" name="file-input" class="form-control-file" onChange={(e)=>setUploadImage(e)} />
                    </div>
                </div>
            </div>
            <Modal.Footer>
                <Button variant="primary" onClick = {()=>onClickSave()}>Guardar producto</Button>
                <Button variant="danger" onClick = {()=>props.handleClose()}>Cerrar</Button>
            </Modal.Footer>
        </div>
    );
}

export default Form;
