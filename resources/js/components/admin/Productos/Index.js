import React, { useState, useEffect  }  from 'react';
import { Button, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';
import '../../../../css/app.css';

import Form from './Form';

import api from "../../../services/axios";

function Index() {

    const [ id, setId ] = useState(null);
    const [ data, setData ] = useState({});
    const [ show, setShow ] = useState(false);
    const [ validar, setValidar ] = useState(false);
    const [ isLoading, setIsLoading ] = useState(false);
    const [ listVendedores, setListVendedores ] = useState([]);

    useEffect(()=> {
        loadData();
    }, []);

    const handleClose = () =>{
        setShow(false);
        setValidar(false)
        setId (null);
        loadData();
    }

    const modal = async () => {
        setShow(true);
    }

    const loadData = () => {
        // codigo para cargar historial
        setIsLoading(true)
  
        api.get("vendedores/get")
        .then((res)=>{
            if(res.success){
                console.log(res.data)
                setListVendedores(res.data)
                setIsLoading(false)
            }
            else {
                setIsLoading(false)
            }
        })
        .catch((error)=>{
            console.log("Error de catch");
            console.log(error.message);
            console.log(error);
            setIsLoading(false)
        })
    }

    const dataUpdate = async (data, valor) => {
        setIsLoading(true)
        console.log("Aqui dataUpdate")
        console.log(data);
    
        var nombres = data.nombres
        var apellidos = data.apellidos
        var edad = data.edad
        var zona = data.zona
    
        const dataForAll = {
            nombres,
            apellidos,
            edad,
            zona,
        }
        setValidar(valor)
        await setId(data.id)
        await setData(dataForAll)
        await setShow(true);
        console.log("Aqui data forAll")
        console.log(dataForAll);
        setIsLoading(false)
        setData({})
    }

    const onClickDelete = (data) =>{
        Swal.fire({
          title: `¿ Quieres eliminar este vendedor ${data.nombres}?`,
          showDenyButton: true,
          confirmButtonText: `Sí`,
          denyButtonText: `No`,
    
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            const res = api.delete("vendedores/"+data.id)
            .then(res=>{
                Swal.fire('Vendedor eliminado correctamente!', '', 'success')
                loadData();
            })
    
          } else if (result.isDenied) {
            Swal.fire('Acción cancelada', '', 'info')
          }
        })
    }

    return (
        <div id="wrapper">
            
            <div class="container-fluid">

            {/* <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Lista de productos</h1>
                <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" onClick = {()=>modal()}>
                    <i class="fas fa-plus fa-sm text-white-50"></i> Crear productos</button>
            </div> */}

            <div class="col-md-12">
                <h3 class="title-5 m-b-35">Lista de productos</h3>


                <button type="button" class="btn btn-primary"  onClick = {()=>modal()}>
                <i class="fas fa-plus fa-sm text-white-50"></i> Crear productos
                </button>

                <hr/>

            </div>

            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive table--no-card m-b-30">
                        <table class="table table-borderless table-striped table-earning  table-data3">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Imagen</th>
                                    <th>Nombre</th>
                                    <th>Estado</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{id > 0 ? 'Editar producto':'Agregar producto'}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form handleClose={()=>handleClose()} data={data} nom={validar} id = {id}/>
                </Modal.Body>
            </Modal>

            </div>
            
        </div>
    );
}

export default Index;