import React, { useState, useEffect } from 'react';
import { Button, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

import Form from "./Form"
import api from "../../../../services/axios"

function Subcategoria() {

    /* const [ listCategoria, setListCategorie ] = useState([]);
    const [ listSubcat, setListSubcatg ] = useState([]);
    const [ dataCategorie, setCategorie ] = useState({});
    const [ form, setForm ] = useState({
        edit: false,
        show:false
    }); */

    const [ id, setId ] = useState(null);
    const [ data, setData ] = useState({});
    const [ show, setShow ] = useState(false);
    const [ validar, setValidar ] = useState(false);
    const [ isLoading, setIsLoading ] = useState(false);
    const [ listCategorie, setListCategorie ] = useState([]);
    const [ listSubCategoria, setListSubCategoria ] = useState([]);

    useEffect(()=>{ onGetData(); },[])

    const handleClose = () =>{
        setShow(false);
        setValidar(false)
        setId (null);
        onGetData();
    }

    const modal = async () => {
        setShow(true);
    }

    const onGetData = (  ) => {

        api.get("subcategory/get")
        .then((res)=>{
            if(res.success){
                setListSubCategoria(res.data)
                setListCategorie(res.catg)
            }
        })
        .catch((error)=>{
            console.log("Error de catch");
            console.log(error.message);
            console.log(error);
        })

    }

    const dataUpdate = async (data, valor) => {
        setIsLoading(true)
        console.log("Aqui dataUpdate")
        console.log(data);

        var nombre = data.nombre
        var categoria_id = data.categoria_id
        var estado = data.estado

        const dataForAll = {
            nombre,
            categoria_id,
            estado,
        }
        setValidar(valor)
        await setId(data.id)
        await setData(dataForAll)
        await setShow(true);
        setIsLoading(false)
        setData({})
    }

    const onClickDelete = (data) =>{
        Swal.fire({
          title: `¿ Quieres eliminar esta categoría ${data.nombre}?`,
          showDenyButton: true,
          confirmButtonText: `Sí`,
          denyButtonText: `No`,

        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            const res = api.delete("subcategory/"+data.id)
            .then(res=>{
                Swal.fire('Categoría eliminada correctamente!', '', 'success')
                onGetData();
            })

          } else if (result.isDenied) {
            Swal.fire('Acción cancelada', '', 'info')
          }
        })
    }

    return (
      <div className="row">
        <div className="col-md-12">
            <h3 className="title-5 m-b-35">Subcategorías</h3>


            <button type="button" className="btn btn-primary"  onClick = {()=>modal()}>
             <i className="fa fa-map-marker"></i>&nbsp; Agregar subcategorías
            </button>

            <hr/>

        </div>




        <div className="col-md-12">
            <div className="table-responsive table--no-card m-b-30">
                <table className="table table-borderless table-striped table-earning  table-data3 border-bottom-primary">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Categoría</th>
                            <th>Estado</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            listSubCategoria.map((item)=>{
                                return(
                                <tr>
                                    <td>{item.id}</td>
                                    <td>{item.nombre}</td>
                                    <td>{item.categorias.nombre}</td>
                                    <td style={item.estado == 1 ? {color:'#00ad5f'} : {color: 'red'}}>{item.estado?"Activo":"Inactivo"}</td>
                                    <td>
                                        <div style={{display: 'flex'}}>
                                        <button className="btn btn-success btn-circle sm" onClick = {()=>dataUpdate(item, true)} style={{marginRight: '10px'}}><i class="fas fa-pen fa-sm"></i></button>
                                        <button className="btn btn-danger btn-circle sm" onClick = {()=>onClickDelete(item)} ><i class="fas fa-trash fa-sm"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>

        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>{id > 0 ? 'Editar subcategoría':'Agregar subcategoría'}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form
                    handleClose={()=>handleClose()}
                    data={data}
                    nom={validar}
                    id = {id}
                    listCategorie={listCategorie}
                />
            </Modal.Body>
        </Modal>


        </div>
    );
}

export default Subcategoria;
