import React, { useState, useEffect  }  from 'react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated'
import Swal from 'sweetalert2';
import { Button, Modal } from 'react-bootstrap';

import { useSelector, useDispatch } from 'react-redux'
import { setCategorias } from '../../../reducer/categoria/categoriaSlice';

import api from "../../../../services/axios";

function Form(props) {

    const [loading, setLoading] = useState(false)

    const [ animatedComponents, setAnimatedComponents ] = useState(makeAnimated)
    const [ id, setId ] = useState(0);
    //const [ categoria , setCategoria ] = useState (null);
    const [ categoriaSelect , setCategoriaSelect ] = useState([]);
    const [ nombre , setNombre ] = useState('');
    const [ active, setActive ] = useState(true);
    const [ error, setError ] = useState(null);


    const dispatch = useDispatch()

    const categorias = useSelector(state => state.categorias.list)

    const getCategory = async () => {
        api
        .get('category/get')
        .then(response=>{
            var res = response.data;
            var catego = [];
            res.map(item=>{
                const data = {
                    value: item.id,
                    label: item.nombre
                }
                catego.push(data);
            })
            dispatch(setCategorias(catego))

            if (props.id > 0){
                console.log(props);

                console.log(props.data.categoria_id);
                let filtro = res.filter(e=>e.id == props.data.categoria_id);
                console.log(filtro);
                const data = {
                    value: filtro[0].id,
                    label: filtro[0].nombre
                }
                setCategoriaSelect(data)
            }
        })
        .catch(error=>{
                console.log(error);
        })
    }



    useEffect(()=>{
        getCategory()
        validar();
        //dispatch(getCategory());
    }, [dispatch]);

    /* axios.get(ip+'admon/ResponsabilidadFiscal').then(response=>{
        var res = response.data
        var fiscal = []
        res.map(item=>{
          const data = {
            value:item.id,
            label:item.id + " - " + item.nombre_res
          }
          fiscal.push(data)
        })
        setFiscal(fiscal)

      }) */

    const MessageError = async (data) => {
        Swal.fire({
          title: 'Error',
          text: data,
          icon: 'warning',
        })
    }

    const MessageSuccess = async (data) => {
        Swal.fire({
          text: data,
          icon: 'success',
        })
    }

    const validar = async () =>{
        if(props.id > 0){
            setNombre(props.data.nombre)
            setActive(props.data.estado)
        }
    }

    const onClickSave = async() => {
        setLoading(true)
        setError("")
        var message = ''
        var error = false
        if (categoriaSelect == ''){
            error = true
            message = "Seleccione una categoría "
        }else if (nombre == ''){
            error = true
            message = "Escriba el nombre de la subcategoría "
        }
        if(error){
            MessageError(message)
        }
        else{
          const data = new FormData()
          data.append('id',id)
          data.append('nombre', nombreSub)
          data.append('categoria_id', categoriaSelect.value)
          data.append('estado', active?1:0);
          api.post("subcategory/create",data)
            .then(res=>{
                console.log("Resultado de entonces");
                console.log(res.message);
                console.log(JSON.stringify(res));
                if(res.success){
                    MessageSuccess("Subcategoría creada correctamente");
                    props.handleClose();
                }else if(props.id > 0){
                    MessageSuccess("Subcategoría editado correctamente");
                    props.handleClose();
                }
                else{
                    setError(JSON.stringify(res.message));
                    setLoading(false);
                    props.handleClose();
                }
            })
            .catch((error)=>{
                console.log("Error de catch");
                console.log(error);
                setError(res.message)
            })
        }
    }



    return(
        <div>

            <div className="container-fluid">
                <div className="row">
                    <div className="col col-md-8 mb-3">
                        <label htmlFor="recipient-name" className="col-form-label">Categoria:</label>
                         <Select
                        value={categoriaSelect}
                        closeMenuOnSelect={true}
                        components={animatedComponents}
                        options={categorias}
                        onChange={(e)=>setCategoriaSelect(e)}
                        placeholder = "Seleccionar categoria"
                        name="colors"
                        />

                       {/*  <select name="select" id="select" class="form-control"
                            value={categoriaSelect} onChange={(event)=>setCategoriaSelect(event.target.value)}>
                            <option></option>
                            {
                                categoria.list.map((item)=>{
                                  return(
                                    <option value={item.id}>{item.nombre}</option>
                                  )
                                })
                            }
                        </select> */}
                    </div>
                    <div className="col col-md-8 mb-3">
                        <label htmlFor="recipient-name" className="col-form-label">Nombre de la subcategoría:</label>
                        <input type="text" className="form-control" placeholder="Ingrese el nombre de la subcategoría" value={nombre} onChange={(event)=>setNombre(event.target.value)}/>
                    </div>
                    <div className="col col-md-8 mb-3">
                        <label htmlFor="text-input" className=" form-control-label">Estado</label>
                        <select name="select" id="select" className="form-control"
                            value={active} onChange={(event)=>setActive(event.target.value)}>
                            <option value="1">Activada</option>
                            <option value="0">Desactivada</option>
                        </select>
                    </div>
                </div>
            </div>
            <Modal.Footer>
                <Button variant="primary" onClick = {()=>onClickSave()}>Guardar subcategoría</Button>
                <Button variant="danger" onClick = {()=>props.handleClose()}>Cerrar</Button>
            </Modal.Footer>
        </div>
    );
}

export default Form;
