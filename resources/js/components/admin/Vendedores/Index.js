import React, { useState, useEffect  }  from 'react';
import { Button, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';
import '../../../../css/app.css';

import Form from './Form';

import api from "../../../services/axios";

function Index() {

    const [ id, setId ] = useState(null);
    const [ data, setData ] = useState({});
    const [ show, setShow ] = useState(false);
    const [ validar, setValidar ] = useState(false);
    const [ isLoading, setIsLoading ] = useState(false);
    const [ listVendedores, setListVendedores ] = useState([]);

    useEffect(()=> {
        loadData();
    }, []);

    const handleClose = () =>{
        setShow(false);
        setValidar(false)
        setId (null);
        loadData();
    }

    const modal = async () => {
        setShow(true);
    }

    const loadData = () => {
        // codigo para cargar historial
        setIsLoading(true)
  
        api.get("vendedores/get")
        .then((res)=>{
            if(res.success){
                console.log(res.data)
                setListVendedores(res.data)
                setIsLoading(false)
            }
            else {
                setIsLoading(false)
            }
        })
        .catch((error)=>{
            console.log("Error de catch");
            console.log(error.message);
            console.log(error);
            setIsLoading(false)
        })
    }

    const dataUpdate = async (data, valor) => {
        setIsLoading(true)
        console.log("Aqui dataUpdate")
        console.log(data);
    
        var nombres = data.nombres
        var apellidos = data.apellidos
        var edad = data.edad
        var zona = data.zona
    
        const dataForAll = {
            nombres,
            apellidos,
            edad,
            zona,
        }
        setValidar(valor)
        await setId(data.id)
        await setData(dataForAll)
        await setShow(true);
        console.log("Aqui data forAll")
        console.log(dataForAll);
        setIsLoading(false)
        setData({})
    }

    const onClickDelete = (data) =>{
        Swal.fire({
          title: `¿ Quieres eliminar este vendedor ${data.nombres}?`,
          showDenyButton: true,
          confirmButtonText: `Sí`,
          denyButtonText: `No`,
    
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            const res = api.delete("vendedores/"+data.id)
            .then(res=>{
                Swal.fire('Vendedor eliminado correctamente!', '', 'success')
                loadData();
            })
    
          } else if (result.isDenied) {
            Swal.fire('Acción cancelada', '', 'info')
          }
        })
    }

    return (
        <div id="wrapper">
            
            <div class="container-fluid">

            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Lista de vendedores</h1>
                <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" onClick = {()=>modal()}>
                    <i class="fas fa-plus fa-sm text-white-50"></i> Crear vendedor</button>
            </div>

            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive border-bottom-primary">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Apellidos</th>
                                    <th>Edad</th>
                                    <th>Zona</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    listVendedores.map((item)=>{
                                        return(
                                            <tr>
                                                <th scope="row">{item.id}</th>
                                                <td>{item.nombres}</td>
                                                <td>{item.apellidos}</td>
                                                <td>{item.edad}</td>
                                                <td>{item.zona == 1 ? 'Huila' : 'Cundinamarca'}</td>
                                                <td>
                                                    <div style={{display: 'flex'}}>
                                                    <button className="btn btn-success btn-circle sm" onClick = {()=>dataUpdate(item, true)} style={{marginRight: '10px'}}><i class="fas fa-pen fa-sm"></i></button>
                                                    <button className="btn btn-danger btn-circle sm" onClick = {()=>onClickDelete(item)} ><i class="fas fa-trash fa-sm"></i></button>
                                                    </div>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{id > 0 ? 'Editar vendedor':'Agregar vendedor'}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form handleClose={()=>handleClose()} data={data} nom={validar} id = {id}/>
                </Modal.Body>
            </Modal>

            </div>
            
        </div>
    );
}

export default Index;