import React, { useState, useEffect  }  from 'react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated'
import Swal from 'sweetalert2';
import { Button, Modal } from 'react-bootstrap';

import api from "../../../services/axios";

function Form(props) {

    const [loading, setLoading] = useState(false)

    const [ animatedComponents, setAnimatedComponents ] = useState(makeAnimated)
    const [ id, setId ] = useState(0);
    const [ nombre, setNombre ] = useState(null);
    const [ apellido, setApellido ] = useState(null);
    const [ edad, setEdad ] = useState(null);
    const [ zona, setZona ] = useState(null);
    const [ error, setError ] = useState(null);


    useEffect(()=>{
        validar();
    }, []);

    const MessageError = async (data) => {
        Swal.fire({
          title: 'Error',
          text: data,
          icon: 'warning',
        })
    }
    
    const MessageSuccess = async (data) => {
        Swal.fire({
          text: data,
          icon: 'success',
        })
    }

    const validar = async () =>{
        if(props.id > 0){
            setNombre(props.data.nombres)
            setApellido(props.data.apellidos)
            setEdad(props.data.edad)
            setZona(props.data.zona)
        }
    }

    const saveVendedor = async() => {
        setLoading(true)
        setError("")
        var message = ''
        var error = false
        if (nombre == null){
          error = true
          message = "Escribe el nombre del vendedor "
        }else if(apellido == null){
          error = true
          message = "Escribe el apellido del vendedor "
        }else if(edad == null){
          error = true
          message = "Escribe la edad del vendedor "
        }else if(zona == null){
          error = true
          message = "Elige la zona del vendedor "
        }
        if(error){
            MessageError(message)
        }
        else{
          const data = new FormData()
          data.append('id',id)
          data.append('nombres', nombre)
          data.append('apellidos', apellido)
          data.append('edad', edad)
          data.append('zona', zona)
          api.post("vendedores/create",data)
            .then(res=>{
                console.log("Resultado de entonces");
                console.log(res.message);
                console.log(JSON.stringify(res));
                if(res.success){
                    MessageSuccess("Vendedor creado correctamente");
                    props.handleClose();
                }else if(props.id > 0){
                    MessageSuccess("Vendedor editado correctamente");
                    props.handleClose();
                }
                else{
                    setError(JSON.stringify(res.message));
                    setLoading(false);
                    props.handleClose();
                }
            })
            .catch((error)=>{
                console.log("Error de catch");
                console.log(error);
                setError(res.message)
            })
        }
    }

    return(
        <div>
     
            <div class="container-fluid">
                <div class="row">
                    <div class="col col-md-8">
                        <label for="recipient-name" className="col-form-label">Nombre del vendedor:</label>
                        <input type="text" className="form-control" placeholder="Ingrese el nombre del vendedor" value={nombre} onChange={(event)=>setNombre(event.target.value)}/>
                    </div>
                    <div class="col col-md-8">
                        <label for="recipient-name" className="col-form-label">Apellido del vendedor:</label>
                        <input type="text" className="form-control" placeholder="Ingrese el apellido del vendedor" value={apellido} onChange={(event)=>setApellido(event.target.value)}/>
                    </div>
                    <div class="col col-md-6">
                        <label for="recipient-name" className="col-form-label">Edad del vendedor:</label>
                        <input type="text" className="form-control" placeholder="Edad del vendedor" value={edad} onChange={(event)=>setEdad(event.target.value)}/>
                    </div>
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Zona del vendedor:</label>
                        <select name="select" id="select" class="form-control" value={zona} onChange={(event)=>setZona(event.target.value)}>
                            <option></option>
                            <option value={1}>HUILA</option>
                            <option value={2}>CUNDINAMARCA</option>
                        </select>
                    </div>
                </div>
            </div>
            <Modal.Footer>
                <Button variant="primary" onClick = {()=>saveVendedor()}>Guardar vendedor</Button>
                <Button variant="danger" onClick = {()=>props.handleClose()}>Cerrar</Button>
            </Modal.Footer>
        </div>
    );
}

export default Form;