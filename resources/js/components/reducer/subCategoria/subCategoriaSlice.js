import { createSlice } from "@reduxjs/toolkit";
import api from '../../../services/axios';
export const subCategoriasSlice = createSlice({
    name: 'subCategorias',
    initialState: {
      list: [],
      isLoading:true
    },
    reducers: {
      setSubcategorias: (state,action) => {
        state.list = action.payload
        state.isLoading = false
      }

    }
  })

  // Action creators are generated for each case reducer function
  export const { setSubcategorias } = subCategoriasSlice.actions

  export default subCategoriasSlice.reducer

/*   export const allCategory =()=>(dispatch)=>{
        api
        .get('category/get')
        .then(response=>{
            dispatch(setCategorias(response.data))
        })
        .catch(error=>{
                console.log(error);
        })
  } */
