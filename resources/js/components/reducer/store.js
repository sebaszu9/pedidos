import { configureStore  } from "@reduxjs/toolkit";
import  categoriasSlice  from "./categoria/categoriaSlice";
import subCategoriasSlice  from "./subCategoria/subCategoriaSlice";
export const store = configureStore({
    reducer:{
        categorias:categoriasSlice,
        subCategorias: subCategoriasSlice
    }
})

