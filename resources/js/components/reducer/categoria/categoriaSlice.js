import { createSlice } from "@reduxjs/toolkit";
import api from '../../../services/axios';
export const categoriasSlice = createSlice({
    name: 'categorias',
    initialState: {
      list: [],
      isLoading:true
    },
    reducers: {
      setCategorias: (state,action) => {
        state.list = action.payload
        state.isLoading = false
      }

    }
  })

  // Action creators are generated for each case reducer function
  export const { setCategorias } = categoriasSlice.actions

  export default categoriasSlice.reducer

/*   export const allCategory =()=>(dispatch)=>{
        api
        .get('category/get')
        .then(response=>{
            dispatch(setCategorias(response.data))
        })
        .catch(error=>{
                console.log(error);
        })
  } */
