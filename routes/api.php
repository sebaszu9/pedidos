<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\VendedoresController;
use App\Http\Controllers\Admin\CategoriasController;
use App\Http\Controllers\Admin\SubCategoriasController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
}); */

/* Route::post('/api/register', [ AuthController::class, 'register']); */
Route::post('register', 'App\Http\Controllers\Admin\AuthController@register');

/* Route::group([ 'prefix' => 'admin', 'middleware' => 'auth:api' ],function(){

    Route::get('/dashboard/index',[ DashboardController::class, 'index']);

}); */

Route::group([ 'prefix' => 'admin', 'middleware' => 'auth:api' ],function(){

    // modulo de vendedores
    Route::prefix("vendedores")->group(function(){
    
        Route::post('/create',[ VendedoresController::class, 'create']);
        Route::get('/get',[ VendedoresController::class, 'getAll']);
        Route::delete('/{id}',[ VendedoresController::class, 'delete']);
    
    });

    Route::prefix("productos")->group(function(){
    
    });

    // modulo de categoria
    Route::prefix("category")->group(function(){
        Route::post('/create',[ CategoriasController::class, 'create']);
        Route::get('/get',[ CategoriasController::class, 'getAll']);
        Route::delete('/{id}',[ CategoriasController::class, 'delete']);
    });
    // modulo de subcategoria
    Route::prefix("subcategory")->group(function(){
        Route::post('/create',[ SubCategoriasController::class, 'create']);
        Route::get('/get',[ SubCategoriasController::class, 'getAll']);
        Route::delete('/{id}',[ SubCategoriasController::class, 'delete']);
    });

});

