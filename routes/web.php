<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\DashboardController;

Route::get('/', function () {
    return view('login');
});

/* Route::get('/encrypted/pass', [ AuthController::class, 'encrypted']); */
Route::post('/api/admin/auth',[ AuthController::class, 'authenticate']);
Route::get('/login', function () { return view('login'); })->name('login');
Route::get('/encrypted/pass', [ AuthController::class, 'encrypted']);
Route::get('/api/admin/logout',[ AuthController::class, 'logout']);

Route::get('/admin', function () { return view('admin'); })->middleware('auth');
Route::get('/admin/{modulo}/{submodulo}', function () { return view('admin'); })->middleware('auth');
Route::get('/admin/{modulo}/{submodulo}/{parte}', function () { return view('admin'); })->middleware('auth');

Route::get('/register', [AuthController::class, 'registerUser']);


