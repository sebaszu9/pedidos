<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vendedores;
use Validator;
use Log;

class VendedoresController extends Controller
{
    //

    public function getAll(){
        try {
          $data = Vendedores::where("deleted",0)->get();
          return response()->json([
            'message' => "Successfully loaded",
            'data'=> $data,
            'success' => true
          ], 200);
        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }
      }

    public function create(Request $request){

        try {

            Log::info($request);
            $id = $request['id'];
            $validator = Validator::make($request->all(), [
                'nombres' => 'required|max:150',
                'apellidos' => 'required|max:150',
                'edad' => 'required|integer',
            ]);

            if ($validator->fails()) {
                return response()->json([ 'message' => $validator->messages(), 'success' => false ], 200);
            }

            $data['nombres'] = $request['nombres'];
            $data['apellidos'] = $request['apellidos'];
            $data['edad'] = $request['edad'];
            $data['zona'] = $request['zona'];

            if($id > 0){
                Vendedores::find($id)->update($data);
            }
            else{
                Vendedores::create($data);
            }
            return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

            } catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }

    }

    public function delete(Request $request){
        Vendedores::where('id', $request['id'])->update([
          'deleted'=>1
        ]);
    }
}
