<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Models\Usuario;
use App\Models\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use Auth;
use Log;


class AuthController extends Controller
{
    //
    public function encrypted()
    {
        $pass = "123456";
        $encript = Hash::make($pass);
        echo $encript;
    }

    public function authenticate(Request $request){

    try {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'message' => $validator->messages(), 'success' => false ], 400);
        }

        $res = Auth::attempt([ 'email' => $request['email'], 'password' => $request['password'] ]);

        if ($res) {
            $user = Auth::user();
            // Log::info($user);
            // Log::info("Auhtin token $user->api_token ");
            $token = $this->updateToken($user);
            // Log::info("Ultimo token $token");
            $response['token'] =  $token;
            $response['success'] = true;
            $response['message'] = "Logueado exitosamente";
        }
        else {
            $response['success'] = false;
            $response['message'] = "Contraseña o email es incorrecto";
        }
        return $response;
    }
    catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }

    }

    public function registerUser()
    {
      return view('register');
    }

    public function register(Request $request)
    {

        Log::info($request);
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
                return response()->json($validator->errors()->toJson(),400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }
  
    public function updateToken($user)
    {
        $token = Str::random(60);
        $user = User::find($user->id);
        $newToken = hash('sha256', $token);

        $user->forceFill([
            'api_token' => $newToken,
        ])->update();

        return $newToken;
    }
  
    public function logout(){
    Auth::logout();
    $response['success'] = true;
    $response['message'] = "Close session";
    return $response;
    }
}
