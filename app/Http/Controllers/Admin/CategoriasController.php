<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categorias;
/* use App\Models\Subcategoria; */
use Validator;
use Log;

class CategoriasController extends Controller
{
    //

    public function getAll(){ 
        try {
          $data = Categorias::where("deleted", 0)->get();
          return response()->json([ 'message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }
    }

    public function create(Request $request){

        try {

          Log::info($request);
          $id = $request['id'];
          $validator = Validator::make($request->all(), [
              'nombre' => 'required',
          ]);

          if ($validator->fails()) {
              return response()->json([ 'message' => $validator->messages(), 'success' => false ], 200);
          }

          if ($request->file('imagen')==null&&$id==0)
          {
            return response()->json([ 'message' => "La imagen es obligatoria", 'success' => false ], 200);
          }

          $data['nombre'] = $request['nombre'];
          $data['estado'] = $request['estado'];
          $data['deleted'] = 0;

          if ($request->file('imagen'))
          {
            // almacena y captura el nombre del archivo
            $image = $request->file('imagen');
            $data['imagen'] = $image->store('categorias','public');
          }


          if($id){
            Categorias::find($id)->update($data);
          }
          else{
            Categorias::create($data);
          }
          return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }

    }

    public function delete(Request $request){
      Categorias::where('id', $request['id'])->update([
          'deleted'=>1
      ]);
    }
}
