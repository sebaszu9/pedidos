<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Usuario;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    //
    public function getAll(){
        $response['data'] =  Usuario::get();
        $response['success'] = true;
        $response['message'] = "Cargo exitosamente";
        return $response;
    }
}
