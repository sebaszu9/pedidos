<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SubCategorias;
use Validator;
use Log;

class SubCategoriasController extends Controller
{
    //

    public function getAll(){ 
        try {
          $data = SubCategorias::with('categorias')->where("deleted", 0)->get();
          return response()->json([ 'message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }
    }

    public function create(Request $request){

        try {

            Log::info($request);
            $id = $request['id'];
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([ 'message' => $validator->messages(), 'success' => false ], 200);
            }

            $data['nombre'] = $request['nombre'];
            $data['categoria_id'] = $request['categoria_id'];
            $data['estado'] = $request['estado'];
            $data['deleted'] = 0;

            if($id){
                SubCategorias::find($id)->update($data);
            }
            else{
                SubCategorias::create($data);
            }
            return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

        } catch (\Exception $e) {
          return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
        }

    }

    public function delete(Request $request){
        Categorias::where('id', $request['id'])->update([
            'deleted'=>1
        ]);
    }
}
