<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategorias extends Model
{
    use HasFactory;

    protected $table = "subcategorias";

    protected $fillable = [
        'nombre',
        'categoria_id',
        'estado',
    ];

    public function categorias(){
        return $this->hasOne("App\Models\Categorias", "id", "categoria_id"); 
    }
}
