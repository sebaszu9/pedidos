<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use HasFactory;

    protected $table = "usuario";

    protected $fillable = [
        'email',
        'password',
        'nombre',
        'apellido',
        'tipo_documento',
        'num_documento',
        'celular',
        'estado', // 1 activo 0 Bloqueado 2 Eliminado
        'role', //1 admin // 2 cliente // 3 domiciliario
        'api_token',
        'remember_token',
        'imagen',
        'token_firebase',
    ];
 
    protected $hidden = [
        'password', 'remember_token', 'api_token', 'role'
    ];
}
